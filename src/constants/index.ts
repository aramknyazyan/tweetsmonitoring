export enum PROVIDERS {
  TWITTER = 'twitter',
  FACEBOOK = 'facebook',
  INSTAGRAM = 'instagram',
}

export const MAX_POSTS_COUNT = 10;
export const SAVE_INTERVAL = 10000;
export const ANOMALY_DIFF = 5;
