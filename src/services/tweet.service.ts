import { Injectable, Logger } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import * as zlib from 'zlib';
import * as util from 'util';
import { Post, PostDocument } from '../models/post.model';
import { Archive, ArchiveDocument } from '../models/archive.model';
import { PROVIDERS, MAX_POSTS_COUNT } from '../constants';
import { checkAnomalyUtil } from '../utils';
import { NotificationService } from './notification.service';
const deflate = util.promisify(zlib.deflate);

@Injectable()
export class TweetService {
  logger: Logger = new Logger(TweetService.name);
  constructor(
    @InjectModel(Post.name) private postModel: Model<PostDocument>,
    @InjectModel(Archive.name) private archiveModel: Model<ArchiveDocument>,
    private readonly notificationService: NotificationService,
  ) {}

  async getTweets(): Promise<Post[]> {
    return this.postModel
      .find({ provider: PROVIDERS.TWITTER })
      .sort({ postCreated: -1 })
      .exec();
  }

  async saveTweets(tag, tweets): Promise<void> {
    this.logger.debug(`(saveTweets) hashtag #${tag}`);
    const postsCount = await this.postModel.countDocuments().exec();
    const postsCountToArchive = postsCount + tweets.length - MAX_POSTS_COUNT;
    this.logger.debug(
      `(saveTweets) postsCountToArchive ${postsCountToArchive}`,
    );
    if (postsCountToArchive > 0) {
      const postsToArchive = await this.postModel
        .find({ provider: PROVIDERS.TWITTER })
        .sort({ postCreated: 1 })
        .limit(postsCountToArchive)
        .exec();
      const archiveData = await deflate(JSON.stringify(postsToArchive));
      const archive = new this.archiveModel({
        provider: PROVIDERS.TWITTER,
        data: archiveData.toString('base64'),
      });
      await archive.save();
      const postsToDeleteIds = postsToArchive.map((p) => {
        return p._id;
      });
      await this.postModel.deleteMany({ _id: { $in: postsToDeleteIds } });
    }
    const postsToSave = tweets.map((tweet) => {
      return {
        postTag: tag,
        postAuthorId: tweet?.user?.id_str || 'unknown',
        postCreated: tweet.created_at,
        provider: PROVIDERS.TWITTER,
        details: tweet,
      };
    });
    await this.postModel.insertMany(postsToSave);
  }

  async checkAnomalyAndNotify(tweetsList, tweetsCount): Promise<void> {
    const anomaly = checkAnomalyUtil(tweetsList, tweetsCount);
    if (anomaly) {
      this.logger.debug(
        `(checkAnomalyAndNotify) anomaly detected: ${JSON.stringify(anomaly)}`,
      );
      await this.notificationService.sendEmail(
        'Anomaly detected',
        anomaly.message,
      );
    }
  }
}
