export * from './tweet.service';
export * from './notification.service';

import { TweetService } from './tweet.service';
import { NotificationService } from './notification.service';

export const Services = [TweetService, NotificationService];
