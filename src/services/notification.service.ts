import { Injectable, Logger } from '@nestjs/common';
import * as nodemailer from 'nodemailer';
import { config } from '../configs';

@Injectable()
export class NotificationService {
  logger: Logger = new Logger(NotificationService.name);
  private readonly transporter;
  constructor() {
    this.transporter = nodemailer.createTransport({
      host: config.APP_SMTP_HOST,
      port: config.APP_SMTP_PORT,
      auth: {
        user: config.APP_SMTP_USER,
        pass: config.APP_SMTP_PASS,
      }
    });
  };

  async sendEmail(subject, text) {
    const emailDetails = {
      from: config.APP_EMAIL_FROM,
      to: config.APP_EMAIL_TO,
      subject,
      text,
    };
    this.transporter.sendMail(emailDetails, (err, info) => {
      if (err) {
        this.logger.debug(`(sendEmail) error ${JSON.stringify(err)}`);
      }
      if (info) {
        this.logger.debug(`(sendEmail) success ${JSON.stringify(info)}`);
      }
    });
  }
}
