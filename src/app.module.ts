import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Models } from './models';
import { Controllers } from './controllers';
import { Services } from './services';
import { config } from './configs';

const dbConnection = `mongodb://${config.APP_DB_HOST}:${config.APP_DB_PORT}/${config.APP_DB_NAME}`;

@Module({
  imports: [
    MongooseModule.forRoot(dbConnection),
    MongooseModule.forFeature(Models),
  ],
  controllers: [...Controllers],
  providers: [...Services],
})
export class AppModule {}
