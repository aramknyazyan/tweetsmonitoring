import { Controller, Get, Param, Post, Logger } from '@nestjs/common';
import * as Twit from 'twit';
import { TweetService, NotificationService } from '../services';
import { config } from '../configs';
import { SAVE_INTERVAL } from '../constants';

@Controller('/api/tweet')
export class TweetController {
  logger: Logger = new Logger(TweetController.name);
  private tweetClient;
  constructor(
    private readonly tweetService: TweetService,
    private readonly notificationService: NotificationService,
  ) {
    this.tweetClient = new Twit({
      consumer_key: config.APP_TWITTER_API_KEY,
      consumer_secret: config.APP_TWITTER_API_KEY_SECRET,
      access_token: config.APP_TWITTER_ACCESS_TOKEN,
      access_token_secret: config.APP_TWITTER_TOKEN_SECRET,
    });
  };

  @Get()
  async getTweets(): Promise<any> {
    this.notificationService.sendEmail('aaa', 'qweqwe');
    return await this.tweetService.getTweets();
  }

  @Get(':tag')
  async getTweetsByTag(@Param('tag') tag: string): Promise<any> {
    return await this.tweetClient.get('search/tweets', {
      q: `#${tag}`,
      count: 10,
    });
  }

  @Post(':tag')
  async saveTweetsByTag(@Param('tag') tag: string): Promise<any> {
    this.logger.debug(`(saveTweetsByTag) Run`);
    const tweetsCountList = [];

    let tweets = [];
    // filtering the public stream by english tweets containing hashtag
    const stream = this.tweetClient.stream('statuses/filter', {
      track: `#${tag}`,
      language: 'en',
    });
    stream.on('tweet', (tweet) => {
      tweets.push(tweet);
    });

    setInterval(() => {
      const tweetsCountPerInterval = tweets.length;
      this.logger.debug(
        `(saveTweetsByTag) save tweets to db and check for anomaly, count: ${tweetsCountPerInterval}`,
      );
      this.tweetService.saveTweets(tag, tweets);
      tweetsCountList.push(tweetsCountPerInterval);
      this.tweetService.checkAnomalyAndNotify(
        tweetsCountList,
        tweetsCountPerInterval,
      );
      tweets = [];
    }, SAVE_INTERVAL);
  }
}
