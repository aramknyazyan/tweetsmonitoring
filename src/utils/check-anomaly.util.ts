import { mean } from 'lodash';
import { ANOMALY_DIFF } from '../constants';

export const checkAnomalyUtil = (tweetsList, tweetsCount) => {
  const meanCount = mean(tweetsList);
  if (meanCount - ANOMALY_DIFF >= tweetsCount) {
    return {
      type: 'low',
      message: `Low number of tweets - ${tweetsCount} vs ${meanCount} mean`,
    };
  } else if (meanCount + ANOMALY_DIFF <= tweetsCount) {
    return {
      type: 'high',
      message: `High number of tweets - ${tweetsCount} vs ${meanCount} mean`,
    };
  }
  return null;
};
