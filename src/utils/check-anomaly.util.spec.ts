import { checkAnomalyUtil } from './check-anomaly.util';

const MOCK_LOW_NUMBER_OF_TWEETS = [10, 9, 2];
const MOCK_LOW_NUMBER = 2;
const MOCK_HIGH_NUMBER_OF_TWEETS = [10, 9, 20];
const MOCK_HIGH_NUMBER = 20;
const MOCK_REGULAR_NUMBER_OF_TWEETS = [10, 9, 10];
const MOCK_REGULAR_NUMBER = 10;

describe('checkAnomalyUtil', () => {
  it('should return an object containing type: low', async () => {
    expect(
      checkAnomalyUtil(MOCK_LOW_NUMBER_OF_TWEETS, MOCK_LOW_NUMBER),
    ).toMatchObject({ type: 'low' });
  });

  it('should return an object containing type: high', async () => {
    expect(
      checkAnomalyUtil(MOCK_HIGH_NUMBER_OF_TWEETS, MOCK_HIGH_NUMBER),
    ).toMatchObject({ type: 'high' });
  });

  it('should return null', async () => {
    expect(
      checkAnomalyUtil(MOCK_REGULAR_NUMBER_OF_TWEETS, MOCK_REGULAR_NUMBER),
    ).toEqual(null);
  });
});
