import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { PROVIDERS } from '../constants';

export type ArchiveDocument = Archive & Document;

@Schema()
export class Archive {
  @Prop({ required: true })
  data: string;

  @Prop({ enum: PROVIDERS, index: true })
  provider: string;

  @Prop({ default: Date.now })
  dateCreated: Date;
}

const ArchiveSchema = SchemaFactory.createForClass(Archive);

export const ArchiveModel = { name: Archive.name, schema: ArchiveSchema };
