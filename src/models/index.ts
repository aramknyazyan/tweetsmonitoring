import { PostModel } from './post.model';
import { ArchiveModel } from './archive.model';

export const Models = [PostModel, ArchiveModel];
