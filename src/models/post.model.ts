import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { PROVIDERS } from '../constants';

export type PostDocument = Post & Document;

@Schema()
export class Post {
  @Prop({ required: true })
  postTag: string;

  @Prop({ required: true })
  postAuthorId: string;

  @Prop({ required: true, index: true })
  postCreated: Date;

  @Prop({ enum: PROVIDERS, index: true })
  provider: string;

  @Prop({ default: Date.now })
  dateCreated: Date;

  @Prop({ type: Object })
  details: any;
}

const PostSchema = SchemaFactory.createForClass(Post);

export const PostModel = { name: Post.name, schema: PostSchema };
