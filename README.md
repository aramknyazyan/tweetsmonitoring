## Description

Application for monitoring real time tweets

## Installation

```bash
$ npm install
```

## Running the app

- Copy .env.default into .env and fill in the variables or skip the step and defaults will be used.
- Start mongodb instance by docker or skip this step if you have your own running mongo.

```bash
# start mongodb
$ docker compose up -d
```

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev
```

## Test

```bash
# unit tests
$ npm run test
```


## HOW TO USE

- In postman call POST `http://localhost:5000/api/tweet/${tagName}` where tagName can be any string (without #) like love
- Check your console and you will see logs regarding running activity
- In postman call GET `http://localhost:5000/api/tweet` for the list of recent tweets
and check your database directly (for example with robomongo or in command line connect)
for some more details, like archives
- Change the constants in src/constants/index.ts to get the anomaly easier (ANOMALY_DIFF) or to have another limit of tweets in collection 
